# Zig GL

experimenting with Zig, WebGL and WASM


# Zig compatibility 
* As of 4.24.2022, this is confirmed to be working with `0.10.0-dev.1929+963ac6091`.
* The build file should be set up to output a `.wasm` file to the `site` folder. Run `zig build -Dweb` to generate the `.wasm` file.
* You can optionaly run `zig build-exe -target wasm32-freestanding src/main.zig` to generate the `.wasm` file. 
