const std = @import("std");
const builtin = @import("builtin");
const passthru = @import("../zig_shaders/test.zig");
const utils = @import("gl/utils.zig");
const gl = @import("gl/constants.zig");
const geo = @import("gl/geometry/core.zig");

const PingPong = @import("gl/framework/pingpong.zig").PingPong;
const Vbo = @import("gl/core/vbo.zig").Vbo;
const DisplayObject = @import("gl/framework/displayobject.zig").DisplayObject;
const Shader = @import("gl/core/shader.zig").Shader;
const Texture = @import("gl/core/texture.zig").Texture;
const DebugTex = @import("gl/core/texture.zig").DebugTex;
const Fbo = @import("gl/core/fbo.zig").Fbo;
const RndGen = std.rand.DefaultPrng;
const helpers = @import("gl/framework/shaderhelpers.zig");

const SIZE = 512;
const allocator = std.heap.page_allocator;

extern fn glClearColor(_: f32, _: f32, _: f32, _: f32) void;
extern fn glViewport(x: c_uint, y: c_uint, width: c_uint, height: c_uint) void;
extern fn getWindowWidth() f32;
extern fn getWindowHeight() f32;
extern fn clearScreen() void;
extern fn enableAlphaBlending() void;
extern fn disableBlending() void;

extern fn logger(data:*f32) void;


const render_vertex = @import("./zig_shaders/render_agent_vs.zig").render_agent_vs;
const render_fragment = @import("./zig_shaders/render_agent_fs.zig").render_agent_fs;
const update_agents = @import("./zig_shaders/update.zig").update;
const diffuse_decay = @import("./zig_shaders/diffuse_decay.zig").diffuse_decay;

const blur = @import("./zig_shaders/blur.zig").blur;
//////////////////////////////////////////////////

/// Physarum simulation based on code from @nicoptere and ported to raw webgl / zig
/// https://github.com/nicoptere/physarum/

pub const Settings = struct {
    decay:f32,
    sa:f32,
    ra:f32,
    so:f32,
    ss:f32
};

/// Handles final render. 
pub const RenderMesh = struct {
    fbo:Fbo,
    mesh:DisplayObject,
};

/// state for the app. 
pub const State = struct {
    render_mesh:RenderMesh,
    trails:PingPong,
    agents:PingPong,
    debug_mesh:DisplayObject,
    settings:Settings,
    blur:Fbo,
    post_mesh:DisplayObject,
    window_width:f32,
    window_height:f32
};

pub var app_state: State = undefined;

// project setup
export fn setup() void {
    
    const t = &app_state;

    const window_width = getWindowWidth();
    const window_height = getWindowHeight();

    t.window_width = window_width;
    t.window_height = window_height;

    const w = @floatToInt(c_uint,window_width);
    const h = @floatToInt(c_uint, window_height);

    // build trails 
    t.trails = PingPong.new(w,h);
    t.trails.set_shader(diffuse_decay);
    
    // build agents ping-pong
    var data = build_texture_data();
    t.agents = PingPong.new_with_data(SIZE,SIZE, data);
    t.agents.set_shader(update_agents);

    // build render
    t.render_mesh = build_rendermesh(w,h);

    // post process blur. 
    t.blur = Fbo.new(w,h);
    t.blur.add_attachment();
    t.blur.build();

    // build the debug mesh.
    t.debug_mesh = utils.build_renderplane();
    
    // build mesh to render post processing. 
    t.post_mesh = build_postprocessing_plane();

    t.settings = Settings {
        .decay = 0.9,
        .sa = 2.0,
        .ra = 4.0, 
        .so = 15.0,
        .ss = 1.1
    };
}

// render loop
export fn draw(time:f32) void {
    const t = &app_state;

    clearScreen();
    disableBlending();
    ///// TRAILS ////// 
    t.trails.bind();
    t.render_mesh.fbo.bindAttachment(0,1);
    t.trails.tex("points",1);
    t.trails.float("decay",t.settings.decay);
    t.trails.unbind();

    ///// AGENTS /////
    t.agents.bind();
    t.trails.bindOutput(1);
    t.agents.tex("data",1);
    t.agents.float("time",time);
    t.agents.float("sa", t.settings.sa);
    t.agents.float("ra", t.settings.ra);
    t.agents.float("so", t.settings.so);
    t.agents.float("ss", t.settings.ss);
    t.agents.unbind();

    //// RENDER //// 
    t.render_mesh.fbo.bind();
    t.agents.bindOutput(1);
    t.render_mesh.mesh.bind();
    t.render_mesh.mesh.tex("uTex0", 1);
    t.render_mesh.mesh.draw();
    t.render_mesh.fbo.unbind();

    ///// OUTPUT //////
    clearScreen();
    enableAlphaBlending();

    t.blur.bind();
    t.trails.bindOutput(0);
    t.post_mesh.bind();
    t.post_mesh.vec2("resolution", @Vector(2,f32){t.window_width,t.window_height});
    t.post_mesh.tex("uTex0",0);
    t.post_mesh.draw();
    t.blur.unbind();

    t.blur.bindAttachment(0,0);
    t.debug_mesh.bind();
    t.debug_mesh.tex("uTex0",0);
    t.debug_mesh.draw();
}


// triggered when window resizes 
// 1st param is the new window width
// 2nd is the new window height 
// TODO Set to named variables,  _ in order to compile for now. 
export fn resize(_: c_int, _: c_int) void {
  
}

//////////////////////////////////////////////////////////////////////

fn build_postprocessing_plane() DisplayObject {

    var verts = Vbo.new();
    verts.set_data(geo.FULLSCREEN_TRIANGLE);
    verts.set_size(2);
    verts.compile();

    var render_plane = DisplayObject.new();

    render_plane.shader.load_vertex(helpers.PASSTHRU_VERTEX_SHADER);
    render_plane.shader.load_fragment(blur);
    render_plane.shader.compile();
    
    render_plane.add_attribute(verts,0,"position");

    return render_plane;
}

fn build_texture_data() []f32 {

    var i:usize = 0;
    var rnd = RndGen.init(0);
    var empty:[]f32 = &[_]f32{};
    const size = 512 * 512 * 4;
    var tex_data = std.heap.page_allocator.alloc(f32, size) catch return empty;
    
    while(i < size) : (i += 4){
        tex_data[i] = rnd.random().float(f32);   
        tex_data[i + 1] = rnd.random().float(f32);   
        tex_data[i + 2] = rnd.random().float(f32);   
        tex_data[i + 3] = 1.0;

    }
    return tex_data;
}

/// Build DisplayObject to handle rendering. 
fn build_rendermesh(w:c_uint,h:c_uint) RenderMesh {

    const pos = build_positions();
    //const pos = utils.initRandomEmptySlice(SIZE * SIZE * 3);

    var position = Vbo.new();
    position.set_data(pos);
    position.compile();

    var uvs = Vbo.new();
    uvs.set_size(2);
    uvs.set_data(build_uvs());
    uvs.compile();

    var mesh = DisplayObject.new();

    mesh.load_shader(render_vertex,render_fragment);
    mesh.add_attribute(position,0,"position");
    mesh.add_attribute(uvs,1,"uv");
    mesh.set_topology(gl.POINTS);
    mesh.set_num_vertices(pos.len / 3);

    var fbo = Fbo.new(w,h);
    fbo.add_attachment();
    fbo.build();

    return RenderMesh {
        .fbo = fbo,
        .mesh = mesh
    };
}

/// build position data for rendering
fn build_positions() []f32 {
    var errors:[]f32 = undefined;
    const count = SIZE * SIZE;
    const pos = std.heap.page_allocator.alloc(f32,count * 3) catch {
        return errors;
    };
    
    // build positions
    var i:usize = 0; 
    while(i < count):(i += 1) {
        pos[i] = 0.0;
    }
    
    return pos;
}

/// Build UV data for rendering 
fn build_uvs() []f32 {
    var errors:[]f32 = undefined;
    const count = SIZE * SIZE;
    const uvs = std.heap.page_allocator.alloc(f32,count * 2) catch {
        return errors;
    };
    
    var i:usize = 0; 
    var idx:f32 = 0.0;
    var size:f32 = @intToFloat(f32,SIZE);
    
    while(i < count):(i += 2) {
        var u =  @mod(idx , size) / size;
        var v = (idx / size) / size;

        uvs[i] = u;
        uvs[i + 1] = v;
        idx += 2;
    }
  
    return uvs;
}
//////////////////////////////////////////////


// main necessary to compile
pub fn main() !void {}
