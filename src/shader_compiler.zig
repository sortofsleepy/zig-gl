const std = @import("std");
const fs = std.fs;

/// Reads a directory of shaders, converts content to zig friendly strings and 
/// outputs zig files that are import-able within zig using the following format 
/// @import("./zig_shaders/<path to file>.zig").<filename>;
///
/// Note that due to the symbol resolution during import, each shader has a unique symbol name based
/// on the file name of the shader. 
///
/// TODO maybe accept command line options
pub fn main() !void {
  
     const cwd = fs.cwd();

    // open shader directory and prepare for iteration. 
    var options = fs.Dir.OpenDirOptions{
        .iterate = true
    };
    const dir = try cwd.openDir("src/shaders",options);
    
    var w = try dir.walk(std.heap.page_allocator);
    defer w.deinit();

    // loop through all the files. 
    while(try w.next()) | entry |{
        //std.debug.print("Hello {s}",.{entry.path});
        var file = try dir.openFile(entry.path,.{});
        defer file.close();

        const start = "pub export const ";
        const end = "\n;";

              const file_size = (try file.stat()).size;

        /////// fill content buffer /////////////// 
    
        const breaker = "\n\\\\";

    
        // do the initial read of the file so we can get things line-by-line
        const file_buffer = try file.readToEndAlloc(std.heap.page_allocator, file_size);
        defer std.heap.page_allocator.free(file_buffer);

        
        // figure out final size that we need for finial string buffer
        var final_size:usize = 0;
        var iter = std.mem.tokenize(u8,file_buffer,"\n");
        while(iter.next()) | line| {
            final_size += line.len;
        }


        ///////////////////

        // writing content of file is done in two steps; 
        // 1. We need to get a line count of the entirety of the content including 
        // breaks so we can create and properly size the content buffer. 
        // 
        // 2. Step 2 is when we actually write the content to the content buffer. 

        // build iter again
        iter = std.mem.tokenize(u8,file_buffer,"\n");

        // figure out total size for the content
        var line_size:usize = 0;
        var breaker_size:usize = 0;
        while(iter.next()) |line| {
            line_size += breaker.len;
            breaker_size += breaker.len;
            line_size += line.len;
        }

        // build content buffer.
        var content_buffer = try std.heap.page_allocator.alloc(u8,line_size);
        defer std.heap.page_allocator.free(content_buffer);


        // build iter one last time. 
        iter = std.mem.tokenize(u8,file_buffer,"\n");

        var lc:usize = 0;
        while(iter.next()) |line| {
            
            // first write line breaks.
            var a: usize = 0;
            while (a < 3) : (a += 1) {
                content_buffer[lc + a] = breaker[a];
            }

            lc += breaker.len;

            a = 0;
            while (a < line.len) : (a += 1) {
                content_buffer[lc + a] = line[a];
            }
            
            lc += line.len;
        }

        ///////////////////////////////

        // last step is to create and fill output buffer with content
        // first get name
        iter = std.mem.tokenize(u8,entry.basename,"\\.");
        var name = iter.next().?;
        const extension = ".zig";
        const connector = " = ";

        var filename = try std.heap.page_allocator.alloc(u8,name.len + extension.len);
        var export_name = try std.heap.page_allocator.alloc(u8,name.len + connector.len);
        
        // build export symbol buffer
        var i:usize = 0; 
        while(i < name.len) : (i += 1){
            export_name[i] = name[i];
        }

        var a:usize = 0;
        while(a < connector.len) : (a += 1){
           export_name[i + a] = connector[a];
        }

        // get file size
        const total_size = start.len + export_name.len + file_size + end.len;

        var buffer = try std.heap.page_allocator.alloc(u8,total_size + breaker_size);
        empty(buffer);
        defer std.heap.page_allocator.free(buffer);

        // fill start
        i = 0;
        while (i < start.len ) : (i += 1) {
            buffer[0 + i] = start[i];
        }

        // add export symbol

        i = 0; 
        while (i < export_name.len ) : (i += 1) {
            buffer[start.len + i] = export_name[i];
        }

        
        // fill content
        i = 0;
        while(i < content_buffer.len) : (i += 1) {
            buffer[start.len + export_name.len + i] = content_buffer[i];
            
            // add line break and closing semi-colon
            buffer[start.len + export_name.len + i + 1] = "\n"[0];
            buffer[start.len + export_name.len + i + 2] = ";"[0];
        }

        ///// BUILD THE FILENAME ////// 
   
        i = 0;
        while(i < name.len) : (i += 1){
            filename[i] = name[i];
        }

        a = 0;
        while(a < extension.len) : (a += 1){
           filename[i + a] = extension[a];
        }

        ///// FINALLY WRITE FILE ///// 

        const output = try cwd.openDir("src/zig_shaders",.{});
        try output.writeFile(filename, buffer);
    
    }


}

/// empties string buffer and replaces nulls with newlines
fn empty(buffer:[]u8) void{
    var len:usize = buffer.len;
    var i:usize = 0;

    while(i < len) : (i += 1){
        buffer[i] = "\n"[0];
    }
}

