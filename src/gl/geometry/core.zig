

/// Vertices for a full screen triangle. 
pub export const FULLSCREEN_TRIANGLE = &[_]f32 {
    -1.0, -1.0, 
    -1.0, 4.0, 
    4.0, -1.0
};