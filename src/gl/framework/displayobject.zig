const Vbo = @import("../core/vbo.zig").Vbo;
const Vao = @import("../core/vao.zig").Vao;
const std = @import("std");
const mat = @import("../core/shader.zig");
const gl = @import("../constants.zig");

// link attributes to a shader. 
extern fn glLinkAttribLocation(    
    vao_id:c_uint, 
    buffer_id: c_uint, 
    shader_id: c_uint, 
    location: c_uint, 
    shader_name: * const u8,
    component_size:c_uint,
    data_type: c_uint,
    normalized: bool,
    stride: c_uint, 
    offset: c_uint) void ;


extern fn glDrawArrays(mode:c_uint, first:c_uint, count:c_uint) void;
extern fn glEnableVertexAttribArray(id:c_uint) void;
extern fn logValue(v:c_uint) void;

/// Defines a attribute on the DisplayObject.
const ObjAttrib = struct {
    shader_location:c_uint,
    vbo:Vbo,
};

pub const DisplayObject = struct {
    
    // reference to shader in webgl land
    shader:mat.Shader,
    attributes:std.ArrayList(ObjAttrib),
    vertexArray:Vao,
    index_buffer:Vbo,
    topology:c_uint,
    num_vertices:c_uint,

    pub fn new() DisplayObject {

        var shader = mat.Shader.new();
        
        return DisplayObject{
            .num_vertices = 3,
            .topology = gl.TRIANGLES,
            .index_buffer = Vbo.new(),
            .vertexArray = Vao.new(),
            .shader = shader,
            .attributes = std.ArrayList(ObjAttrib).init(std.heap.page_allocator)
        };
    }

    /// Loads the shader for the DisplayObject. 
    pub fn load_shader(self:*DisplayObject, vertex:[]const u8,fragment:[] const u8) void {
        self.shader.load_vertex(vertex);
        self.shader.load_fragment(fragment);
        self.shader.compile();
    }

    /// sets the number of vertices for the mesh.
    pub fn set_num_vertices(self:*DisplayObject, num:c_uint) void {
        self.num_vertices = num;
    }

    /// Pass-thru uniform setter for textures.
    pub fn tex(self:*DisplayObject, name:[]const u8, texture_id:c_uint) void {
        self.shader.tex(name,texture_id);
    }

    /// pass-thru uniform setter for float values. 
    pub fn float(self:*DisplayObject, name:[]const u8, value:f32) void {
        self.shader.float(name,value);
    }

    /// pass-thru uniform setter for vec2 
    pub fn vec2(self:*DisplayObject, name:[]const u8, value:@Vector(2,f32)) void {
        self.shader.vec2(name,value);
    }

    /// Adds an attribute to the object. Makes an assumption as to attribute size, 
    /// pass in the shader location and name of the attribute. 
    pub fn add_attribute(self:*DisplayObject, vbo:Vbo, location:c_uint, attrib_name:[]const u8) void {

        glLinkAttribLocation(
            self.vertexArray.id,
            vbo.id,
            self.shader.id,
            location,
            &attrib_name[0],
            vbo.component_size,
            gl.FLOAT,
            false,
            0,
            0
        );

        var attrib = ObjAttrib {
            .vbo = vbo,
            .shader_location = location 
        };

        self.attributes.append(attrib) catch return;
    }

    pub fn debug(self:*DisplayObject) void {
        logValue(self.attributes.items.len);
    }

    /// Binds object for drawing. Done like this so we can 
    // more easily call uniform setters. 
    pub fn bind(self:*DisplayObject) void {
        //bind shader
        self.shader.bind();

        // bind attributes 
        self.vertexArray.bind();
    
    }

    pub fn set_topology(self:*DisplayObject, topology:c_uint) void {
        self.topology = topology;
    }


    /// Renders the Display object. essentially a passthru to glDrawArrays.
    pub fn draw_arrays_raw(self:*DisplayObject, first:c_uint, count:c_uint) void {
     
        // enable attributes
        for (self.attributes.items) | attrib | {
            glEnableVertexAttribArray(attrib.shader_location);
        }
    

        // TODO alter draw command depending on instanced, indexbuffer, etc. 
        glDrawArrays(self.topology,first,count);
    }

    /// Draws the object based on the current settings. 
    pub fn draw(self:*DisplayObject) void {
        // enable attributes
        for (self.attributes.items) | attrib | {
            glEnableVertexAttribArray(attrib.shader_location);
        }
    
        // TODO alter draw command depending on instanced, indexbuffer, etc. 
        glDrawArrays(self.topology,0,self.num_vertices);
    }
    
};