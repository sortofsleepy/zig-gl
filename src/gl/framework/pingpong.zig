const std = @import("std");
const geo = @import("../geometry/core.zig");
const shader_helpers = @import("./shaderhelpers.zig");

const Fbo = @import("../core/fbo.zig").Fbo;
const DisplayObject = @import("./displayobject.zig").DisplayObject;
const Texture = @import("../core/texture.zig").Texture;
const Vbo = @import("../core/vbo.zig").Vbo;
const utils = @import("../utils.zig");
extern fn glViewport(x: c_uint, y: c_uint, width: c_uint, height: c_uint) void;
extern fn glClearColor(r:f32,g:f32, b:f32, a:f32) void;
extern fn clearCommonBits() void;
extern fn logValue(v:c_uint) void;

pub const PingPong = struct {
    buffers:[2]Fbo,
    flag: u32,
    render_plane:DisplayObject,
    resolution:@Vector(2,f32),
    clear_color:@Vector(4,f32),

    pub fn new(width:c_uint,height:c_uint) PingPong {

        const w = @intToFloat(f32,width);
        const h = @intToFloat(f32,height);

        var fbo = Fbo.new(width,height);
        fbo.add_attachment();
        fbo.build();

        var fbo2 = Fbo.new(width,height);
        fbo2.add_attachment();
        fbo2.build();

        var verts = Vbo.new();
        verts.set_data(geo.FULLSCREEN_TRIANGLE);
        verts.set_size(2);
        verts.compile();
        var plane = DisplayObject.new();
        plane.add_attribute(verts,0,"position");

        return PingPong {
            .clear_color = @Vector(4,f32){
                0.0,
                0.0,
                0.0,
                1.0
            },
            .flag = 0,
            .render_plane = plane,
            .resolution = @Vector(2,f32){w,h},
            .buffers = [2] Fbo {
                fbo,
                fbo2
            }
        };
    }

     pub fn new_with_data(width:c_uint,height:c_uint, data:[]f32) PingPong {

        const w = @intToFloat(f32,width);
        const h = @intToFloat(f32,height);

        var data2 = utils.initEmptySlice(width * height * 4);

        std.mem.copy(f32,data2,data);

        var tex1 = Texture.new_with_data(width,height,data);
        var tex2 = Texture.new_with_data(width,height,data2);

        var fbo = Fbo.new(width,height);
        fbo.add_attachment_texture(tex1);
        fbo.build();

        var fbo2 = Fbo.new(width,height);
        fbo2.add_attachment_texture(tex2);
        fbo2.build();

        var verts = Vbo.new();
        verts.set_data(geo.FULLSCREEN_TRIANGLE);
        verts.set_size(2);
        verts.compile();
        var plane = DisplayObject.new();
        plane.add_attribute(verts,0,"position");

        return PingPong {
            .clear_color = @Vector(4,f32){
                0.0,
                0.0,
                0.0,
                1.0
            },
            .flag = 0,
            .render_plane = plane,
            .resolution = @Vector(2,f32){w,h},
            .buffers = [2] Fbo {
                fbo,
                fbo2
            }
        };
    }

    /// clears Fbos 
    pub fn clear(self:*PingPong) void {
        self.update();
        self.update();
    }
    
    /// sets the shader for the ping pong buffer
    pub fn set_shader(self:*PingPong, shader:[]const u8) void {
        self.render_plane.shader.load_vertex(shader_helpers.PASSTHRU_VERTEX_SHADER);
        self.render_plane.shader.load_fragment(shader);
        self.render_plane.shader.compile();
    }

    /// Binds the output for rendering. 
    pub fn bindOutput(self:*PingPong, index:c_uint) void {
        self.buffers[self.flag].bindAttachment(0,index);
    }

    /// Passthru uniform function for floating point values. 
    pub fn float(self:*PingPong, name:[] const u8, value:f32) void {
        self.render_plane.float(name,value);
    }

    /// Passthru texture uniform. 
    pub fn tex(self:*PingPong, name:[] const u8, value:c_uint) void {
        self.render_plane.tex(name,value);
    }

    /// Passthru vec2 function. 
    pub fn vec2(self:*PingPong, name:[]const u8, value:@Vector(2,f32)) void {
        self.render_plane.vec2(name,value);
    }

    /// Binds the ping pong buffer 
    pub fn bind(self:*PingPong) void {
  
        const w = @floatToInt(c_uint,self.resolution[0]);
        const h = @floatToInt(c_uint,self.resolution[1]);
      
        self.buffers[1 - self.flag].bind();

            // set the viewport, clearcolor and clear the common 
            // bits to render to the texture. 
            glViewport(0,0,w,h);
            glClearColor(
                self.clear_color[0],
                self.clear_color[1],
                self.clear_color[2],
                self.clear_color[3]
            );
            clearCommonBits();


            // bind opposite buffer as input.
            self.buffers[self.flag].bindAttachment(0,0);

            self.render_plane.bind();
            
            // send previous texture
            self.render_plane.shader.tex("uTex0",0);

            // send resolution
            self.render_plane.shader.vec2("resolution",self.resolution);
    }

    /// Unbinds the ping-pong buffer and renders things to current fbo.
    pub fn unbind(self:*PingPong) void {
        
        self.render_plane.draw_arrays_raw(0,3);
        self.buffers[self.flag].unbind();
        self.flag = 1 - self.flag;
    }
};

