////// HELPER SHADERS /////////

// passthru for full screen effects 
pub export const PASSTHRU_VERTEX_SHADER = 
  \\ #version 300 es
  \\ in vec2 position;
  \\ out vec2 vUv;
  \\ const vec2 scale = vec2(0.5,0.5);
  \\ 
  \\ void main(){
  \\    vUv = position.xy * scale + scale;
  \\    gl_Position = vec4(position,0.0,1.);
  \\}
  ;


  pub export const PASSTHRU_FRAGMENT_SHADER =
  \\ #version 300 es
  \\ precision highp float;
  \\ uniform sampler2D uTex0;
  \\ in vec2 vUv;
  \\ out vec4 glFragColor;
  \\ void main(){
  \\    vec4 dat = texture(uTex0,vUv);
  \\    glFragColor = dat;
  \\}
  ;