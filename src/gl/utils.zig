const std = @import("std");
const Vbo = @import("core/vbo.zig").Vbo;
const geo = @import("geometry/core.zig");
const DisplayObject = @import("framework/displayobject.zig").DisplayObject;
const helpers = @import("framework/shaderhelpers.zig");

const RndGen = std.rand.DefaultPrng;


/// Initializes a variable length slice.
pub fn initEmptySlice(size:usize) []f32 {
    var empty = std.ArrayList(f32).init(std.heap.page_allocator);
    var i:usize = 0;
    while(i < size){
        empty.append(0.0) catch return empty.items;
        i += 1;
    }
    return empty.items;
}

/// Same as initEmptySlice but inserts random values instead of 0s
pub fn initRandomEmptySlice(size:usize) []f32 {
    var empty = std.ArrayList(f32).init(std.heap.page_allocator);
    var i:usize = 0;
    var rnd = RndGen.init(0);

    while(i < size){
        var num = rnd.random().float(f32);    
        empty.append(num) catch return empty.items;
        i += 1;
    }
    return empty.items;
}


/// Helper to build a full screen render plane.
pub fn build_renderplane() DisplayObject {
    var verts2 = Vbo.new();
    verts2.set_data(geo.FULLSCREEN_TRIANGLE);
    verts2.set_size(2);
    verts2.compile();

    var render_plane = DisplayObject.new();

    // load an alternate shader 
    var frag_shader = 
        \\ #version 300 es
        \\ precision highp float;
        \\ uniform sampler2D uTex0;
        \\ in vec2 vUv;
        \\ uniform vec2 resolution;
        \\ out vec4 glFragColor;
        \\ void main(){
        \\    vec4 dat = texture(uTex0,vUv);
        \\    glFragColor = dat;
        \\}
        ;

    render_plane.shader.load_vertex(helpers.PASSTHRU_VERTEX_SHADER);
    render_plane.shader.load_fragment(frag_shader);
    render_plane.shader.compile();
    
    render_plane.add_attribute(verts2,0,"position");

    return render_plane;
}


pub fn build_renderplane_with_shader(shader:[]const u8) DisplayObject {
    var verts2 = Vbo.new();
    verts2.set_data(geo.FULLSCREEN_TRIANGLE);
    verts2.set_size(2);
    verts2.compile();

    var render_plane = DisplayObject.new();

    render_plane.shader.load_vertex(helpers.PASSTHRU_VERTEX_SHADER);
    render_plane.shader.load_fragment(shader);
    render_plane.shader.compile();
    
    render_plane.add_attribute(verts2,0,"position");

    return render_plane;
}