const std = @import("std");

extern fn glCreateBuffer() c_uint;
extern fn glCompileBuffer(id:c_uint, source: *const f32, len:usize) void;
extern fn glCompileBufferWithVao(vao_id:c_uint, id:c_uint, source: *const f32, len:usize) void;


/// A wrapper around the concept of a Vertex Buffer Object. 
pub const Vbo = struct {
    id:c_uint,
    vertices:std.ArrayList(f32),

    //A number specifying the number of components per vertex attribute. Must be 1, 2, 3, or 4. ie positions have a size of 3 for x,y,z
    component_size:c_uint,
    has_data:bool,
    normalized:bool, 
    stride:c_uint,
    offset:c_uint,

    // total size of all elements in the buffer
    data_size:c_uint,

    pub fn new() Vbo {

        return Vbo {
            .data_size = 0,
            .has_data = false,
            .stride = 0, 
            .offset = 0,
            .normalized = false,
            .component_size = 3,
            .id = glCreateBuffer(),
            .vertices = std.ArrayList(f32).init(std.heap.page_allocator)
        };
    }

    /// sets the component size of the data in the vbo. 
    pub fn set_size(self:*Vbo, size:c_uint) void {
        self.component_size = size;
        self.has_data = true;
    }

    /// Sets data on top of the buffer
    pub fn set_data(self:*Vbo, vertices:[]const f32) void {
         for(vertices) | value | {
            self.vertices.append(value) catch return;
         }

         self.data_size = vertices.len;
    }

    /// compiles the Vbo on the client side. 
    pub fn compile(self:*const Vbo)void{
        glCompileBuffer(self.id,&self.vertices.items[0],self.vertices.items.len);
    }

    /// Same as compileBuffer but associates buffer with the specified Vao id. 
    pub fn compileWithVao(self:*const Vbo, vao_id:c_uint) void {
        glCompileBufferWithVao(vao_id, self.id,&self.vertices.items[0],self.vertices.items.len);
    }

    
};