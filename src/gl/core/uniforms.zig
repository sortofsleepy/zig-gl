
extern fn glUniform1i(shader_id:c_uint, name:*const u8, value:c_uint) void;
extern fn glUniform2fv(shader_id:c_uint, name:*const u8, v1:f32, v2:f32) void;
extern fn glUniform1f(shader_id:c_uint, name:*const u8, value:f32) void;

/// Sets value for a texture uniform 
pub fn tex(shader_id:c_uint, name:*const u8, value:c_uint) void {
    glUniform1i(shader_id,name,value);
}   


/// Sets value for a texture uniform 
pub fn vec2(shader_id:c_uint, name:*const u8, value:@Vector(2,f32)) void {
    glUniform2fv(shader_id,name,value[0],value[1]);
}   


pub fn float(shader_id:c_uint,name:*const u8, value:f32) void {
    glUniform1f(shader_id,name,value);
}