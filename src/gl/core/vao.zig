extern fn glCreateVao() c_uint; 
extern fn glBindVertexArray(id:c_uint) void;

pub const Vao = struct {
    id:c_uint, 

    pub fn new() Vao {
        return Vao {
            .id = glCreateVao()
        };
    }

    /// Binds the vao for use
    pub fn bind(self:*Vao) void {
        glBindVertexArray(self.id);
    }

};