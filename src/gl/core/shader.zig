const uniforms = @import("uniforms.zig");
const shader_helpers = @import("../framework/shaderhelpers.zig");

extern fn glCompileShaderProgram(
  vertex_shader:*const u8,
  fragment_shader:*const u8,
  program_id:u32
) void;

extern fn glBindShader(id:c_uint) void;
extern fn glCreateProgram() c_uint;

const ShaderSource = struct {
  source:?*const u8
};

/// A wrapper around the concept of a Vertex Buffer Object. 
pub const Shader = struct {
    id:c_uint,
    vertex_shader:ShaderSource,
    fragment_shader:ShaderSource,

    pub fn new() Shader {
      return Shader {
          .id = glCreateProgram(),
          .vertex_shader = ShaderSource{
            .source = null
          },
          .fragment_shader = ShaderSource{
            .source = null
          }
      };
    }

    /// create shader object while passing in sources. 
    pub fn create_shader(vertex:[]const u8, fragment:[]const u8) Shader {
       return Shader {
          .id = glCreateProgram(),
          .vertex_shader = ShaderSource{
            .source = &vertex[0]
          },
          .fragment_shader = ShaderSource{
            .source = &fragment[0]
          }
      };
    }

    /// binds the shader
    pub fn bind(self:*Shader) void{
      glBindShader(self.id);
    }

    /// returns the id of the shader on the client side. 
    pub fn get_id(self:*Shader) c_uint {
      return self. id;
    }

    /// loads a vertex shader. Remember to call compile.
    pub fn load_vertex(self:*Shader, source:[]const u8) void {
      self.vertex_shader.source = &source[0];
    }

    /// loads a fragment shader. Remember to call compile.
    pub fn load_fragment(self:*Shader, source:[]const u8) void {
      self.fragment_shader.source = &source[0];
    }

    /// compiles shader program on WebGL side.
    pub fn compile(self:*Shader) void {
      // if a shader hasn't been set, load passthru shaders. 
      glCompileShaderProgram(
        self.vertex_shader.source orelse &shader_helpers.PASSTHRU_VERTEX_SHADER[0],
        self.fragment_shader.source orelse &shader_helpers.PASSTHRU_FRAGMENT_SHADER[0],
        self.id
      );
    }

    /// sends a texture to shader. 
    pub fn tex(self:*Shader, name:[]const u8,value:c_uint) void {
      uniforms.tex(self.id,&name[0],value);
    }

    /// sends a vec2 uniform to the shader 
    pub fn vec2(self:*Shader, name:[]const u8,value:@Vector(2,f32)) void {
      uniforms.vec2(self.id,&name[0],value);
    }

    /// sets a flow uniform to the shader. 
    pub fn float(self:*Shader, name:[] const u8, value:f32) void {
      uniforms.float(self.id,&name[0],value);
    }
};

