const gl = @import("../constants.zig");
const std = @import("std");
const utils = @import("../utils.zig");

extern fn glBindTexture2d(id:u32, index:u32) void;
extern fn glTexImage2D(tex_id:c_uint, internalFormat:u32, width: u32, height:u32, format:u32, texType:u32, data:*const f32,len:c_uint) void;
extern fn glTexImage2DFullscreen(internalFormat:u32, format:u32, texType:u32, data:*const f32) void;
extern fn glTexParam(filterVal:c_uint) void;
extern fn logValue(v:c_uint) void;
extern fn glDebugTex(id:c_uint,data:*f32,len:c_uint) void;

// create texture will return index of acctual texture in global texture array.
extern fn glCreateTexture() c_uint;

// A wrapper around the basic idea of a texture. 
pub const Texture = struct {
    id:c_uint,
    width: c_uint, 
    height: c_uint,
    data:[]f32,


    /// Creates a new Texture 
    pub fn new(width:c_uint, height:c_uint) Texture{        

        var tex = glCreateTexture();

        // initialize texture with some data. Assumes RGBA.
        var size = width * height * 4;
        var data = utils.initEmptySlice(size);
        glTexImage2D(tex,gl.RGBA32F, width, height, gl.RGBA, gl.FLOAT,&data[0],size);
        
        return Texture {
            .data = data,
            .id = tex,
            .width = width, 
            .height = height
        };
    }

    pub fn new_with_data(width:c_uint, height:c_uint, data:[]f32) Texture{        

        var tex = glCreateTexture();

    
        glTexImage2D(tex,gl.RGBA32F, width, height, gl.RGBA, gl.FLOAT,&data[0],data.len);
        
        return Texture {
            .data = data,
            .id = tex,
            .width = width, 
            .height = height
        };
    }

    /// same as new but initializes a full screen texture. 
    pub fn new_fullscreen() Texture{        
        var tex = glCreateTexture();

        return Texture {
            .id = tex,
            .width = 0, 
            .height = 0
        };
    }

    /// resizes texture to be full screen (whatever the window size is at the time it's called. )
    pub fn resize_fullscreen(self:*Texture) void {
        glTexImage2DFullscreen(gl.RGBA32F, gl.RGBA, gl.FLOAT,&self.data[0]);
    }

    // binds the texture to the specified texture index.
    pub fn bind(self:Texture, index:u32) void {
        glBindTexture2d(self.id, index);
    }

    // Writes texture data. 
    // Texture must be bound prior to calling this function. 
    pub fn texImage2D(internalFormat:u32, width: u32, height:u32, format:u32, texType:u32, data:*const u8,len:c_uint) void {
        glTexImage2D(internalFormat, width, height, format, texType, data,len);
    }


};


