const Texture = @import("./texture.zig").Texture;
const std = @import("std");

extern fn glCreateFramebuffer() c_uint;
extern fn glBindFramebuffer(c_uint) void;
extern fn unbindFramebuffer() void;
extern fn glFramebufferAttachment(fbo_id:c_uint, texture_id:c_uint) void;
extern fn glViewport(x: c_uint, y: c_uint, width: c_uint, height: c_uint) void;
extern fn glClearColor(r:f32,g:f32, b:f32, a:f32) void;
extern fn clearCommonBits() void;

/// A wrapper around the concept of a Framebuffer Object. 
pub const Fbo = struct {
    id:c_uint, 
    width:c_uint,
    height:c_uint,
    attachments:std.ArrayList(Texture),
    clear_color:@Vector(4,f32),
    should_clear:bool,
    
    pub fn new(width:c_uint,height:c_uint) Fbo {
        return Fbo {
            .attachments = std.ArrayList(Texture).init(std.heap.page_allocator),
            .id = glCreateFramebuffer(),
            .width = width,
            .height = height,
            .should_clear = true,
            .clear_color = @Vector(4,f32){
                0.0,
                0.0,
                0.0,
                1.0
            },
        };
    }
    
    /// Adds an attachment with the default settings.
    pub fn add_attachment(self:*Fbo) void {
        self.attachments.append(Texture.new(self.width,self.height)) catch return;
    }

    /// adds the specified texture to the Fbo. 
    pub fn add_attachment_texture(self:*Fbo, tex:Texture) void {
        self.attachments.append(tex) catch return;
    }

    /// Binds the framebuffer. 
    pub fn bind(self:*Fbo) void {
        glBindFramebuffer(self.id);
 
        // set the viewport, clearcolor and clear the common 
        // bits to render to the texture. 
        glViewport(0,0,self.width,self.height);
      
        if(self.should_clear == true){
            glClearColor(
                self.clear_color[0],
                self.clear_color[1],
                self.clear_color[2],
                self.clear_color[3]
            );
            clearCommonBits();
        }

    }

    pub fn toggle_clear(self:*Fbo) void {
        self.should_clear = false;
    }

    /// Unbinds the framebuffer. 
    pub fn unbind(_:*Fbo) void {
        unbindFramebuffer();
    }

    /// Binds attachment for rendering. 
    /// First parameter is the attachment index, 
    /// Second parameter is the texture index to bind to. 
    pub fn bindAttachment(self:*Fbo, attachment_index:c_uint, index:c_uint) void {
        if(self.attachments.items.len > 0){
            self.attachments.items[attachment_index].bind(index);
        }
    }

    /// Constructs a framebuffer backed by the sepcified attachment.    
    pub fn build(self:Fbo) void {
        // buffer all of the attachments onto the framebuffer 
        for(self.attachments.items) | *attachment | {
            glFramebufferAttachment(self.id,attachment.id);
        }
    }
};

