pub export const VERTEX_SHADER: c_uint = 35633;
pub export const FRAGMENT_SHADER: c_uint = 35632;
pub export const ARRAY_BUFFER: c_uint = 34962;
pub export const TRIANGLES: c_uint = 4;
pub export const POINTS:c_uint = 0;
pub export const STATIC_DRAW: c_uint = 35044;
pub export const DEPTH_TEST: c_uint = 2929;
pub export const LEQUAL: c_uint = 515;
pub export const COLOR_BUFFER_BIT: c_uint = 16384;
pub export const DEPTH_BUFFER_BIT: c_uint = 256;
 
pub export const FLOAT: c_uint = 5126;
pub export const UNSIGNED_BYTE: c_uint = 5121;
pub export const RGBA: c_uint = 6408;
pub export const RGBA32F: c_uint = 34836;

pub export const CLAMP_TO_EDGE = 33071;
pub export const NEAREST = 9728;
pub export const LINEAR = 9729;

pub export const TEXTURE_WRAP_S = 10242;
pub export const TEXTURE_WRAP_T = 10243;
pub export const PACK_ALIGNMENT = 3333;

pub export const MAG_FILTER = 10240;
pub export const MIN_FILTER = 10241;