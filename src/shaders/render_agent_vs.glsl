#version 300 es


in vec3 position;
in vec2 uv;
uniform sampler2D uTex0;
out vec2 vUv;
void main(){

    vUv = uv;
    vec2 uv = texture(uTex0,uv).xy;
    gl_Position = vec4(uv * 2.0 - 1.,0.,1.);
    gl_PointSize = 2.0 * (uv.x + uv.y * 2.0);

}