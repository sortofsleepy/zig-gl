#version 300 es
precision highp float;
const float blurPower = 40.4;
const int sampleCount = 20;

uniform vec2 resolution;
uniform sampler2D uTex0;
in vec2 vUv;
out vec4 glFragColor;

void main(){

    vec2 uv = vUv;
    vec2 direction = vec2(1.0 / resolution.x, 0.0); 
    vec2 direction2 = vec2(0.0, 1.0 / resolution.y);

    vec2 vUV = gl_FragCoord.xy / resolution.xy;
    vec4 orig = texture(uTex0, vUV );
     
	vec3 c = vec3(0.0, 0.0, 0.0);
	
    float f = 0.001;
	
    for(int i = -10; i < sampleCount; ++i)
    {
        c += texture(uTex0, uv + float(i * -1) * direction ).rgb * f;
	}
    
     for(int i = sampleCount; i > 0; i--)
    {
        c += texture(uTex0, uv + float(i) * direction2 ).rgb * f;
	}   

    vec4 fin = vec4(c * blurPower,1.0);
    fin = vec4(fin.ggg,1.);

    //glFragColor = vec4(orig.ggg * vec3(1.0,0.0,0.0),1.);
    glFragColor = fin * vec4(orig.a, orig.a,0.0,1.) + vec4(orig.a, 0.0,0.0,1.);;

}

