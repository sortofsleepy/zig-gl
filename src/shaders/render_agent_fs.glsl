#version 300 es
precision highp float;
out vec4 glFragColor;
void main(){
    float d = 1.0 - length(0.5 - gl_PointCoord.xy);
    glFragColor = vec4(d,d,d,1.0);
    //glFragColor = vec4(1.);
}