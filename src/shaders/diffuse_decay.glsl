#version 300 es 
precision highp float;

uniform sampler2D points;
uniform sampler2D uTex0;
uniform vec2 resolution;
uniform float time;
uniform float decay;
in vec2 vUv;
out vec4 glFragColor;
void main(){

    vec2 res = 1. / resolution;
    float pos = texture(points, vUv).r;

    //accumulator
    float col = 0.;

    //blur box size
    const float dim = 1.;

    //weight
    float weight = 1. / pow( 2. * dim + 1., 2. );

    float offPos = 0.0;
    for( float i = -dim; i <= dim; i++ ){

        for( float j = -dim; j <= dim; j++ ){

            vec3 val = texture( uTex0, fract( vUv+res*vec2(i,j) ) ).rgb;
            col += val.r * weight + val.g * weight * .5;
            offPos = val.g * val.b * val.r;
        }
    }

    vec4 fin = vec4( pos * decay, col * decay, .5, offPos );
    //glFragColor = clamp( fin, 0.01, 1. );
    glFragColor = fin;

}