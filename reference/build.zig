

    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.

    const wasm = b.option(bool, "web", "Build for the web.");

    if(wasm == null){
        const target = b.standardTargetOptions(.{});

        // Standard release options allow the person running `zig build` to select
        // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall.
        const mode = b.standardReleaseOptions();


        //// BUILD EXE /////
        const exe = b.addExecutable("zig_gl", "src/main.zig");
        exe.setTarget(target);
        exe.setBuildMode(mode);
        exe.addPackagePath("glfw", "libs/mach-glfw/src/main.zig");
        glfw.link(b, exe, .{});

        exe.install();

        ///// RUN STEP /////
        const run_step = exe.run();
        const step = b.step("run", "Runs executable");
        step.dependOn(&run_step.step);

    }else {
        const exe = b.addExecutable("zig_gl", "src/main.zig");
        exe.setTarget(.{.cpu_arch = .wasm32, .os_tag = .freestanding});
        exe.setOutputDir("site");
        
        b.default_step.dependOn(&exe.step);
    }

  