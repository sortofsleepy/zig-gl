// declare in javascript
extern fn test_fn(_:f32) void;

// this is called from javascript.
export fn onAnimationFrame() void {
    test_fn(30.0);
}

// run 
// zig build-exe -target wasm32-freestanding src/main.zig
// to compile wasm package.