const std = @import("std");
const builtin = @import("builtin");
const obj = @import("gl/framework/displayobject.zig");
const Vbo = @import("gl/core/vbo.zig").Vbo;
const geo = @import("gl/geometry/core.zig");
const PingPong = @import("gl/framework/pingpong.zig").PingPong;
const Fbo = @import("gl/core/fbo.zig").Fbo;
const Texture = @import("gl/core/texture.zig").Texture;
const Shader = @import("gl/core/shader.zig").Shader;
const uniforms = @import("gl/core/uniforms.zig");

extern fn glClearColor(_: f32, _: f32, _: f32, _: f32) void;
extern fn glViewport(x: c_uint, y: c_uint, width: c_uint, height: c_uint) void;
extern fn glViewportFullscreen(_: f32, _: f32) void;
extern fn glDebug(val:c_uint) void;
extern fn consoleLog() void;

pub const is_web = builtin.arch == builtin.Arch.wasm32;
//////////////////////////////////////////////////


pub const State = struct {
    plane:obj.DisplayObject,
    render_plane:obj.DisplayObject,
    fbo:Fbo,
    pingpong:PingPong
};

pub var aPingPong_state: State = undefined;


// project setup
export fn setup() void {
    const t = &aPingPong_state;

    t.pingpong = build_pingpong();
    ///// BUILD FBO TEST OBJ ///////
    t.plane = obj.DisplayObject.new();

    var verts = Vbo.new();
    verts.add_vertices(geo.FULLSCREEN_TRIANGLE);
    verts.set_size(2);
    verts.compile();

    t.plane.shader.compile();
    t.plane.add_attribute(verts,0,&"position"[0]);

    ///// BUILD FBO ///////
    t.fbo = Fbo.new(1024,768);
    t.fbo.add_attachment();
    t.fbo.build();

    //// BUILD RENDER_PLANE //// 
    t.render_plane = build_renderplane();

}

fn build_pingpong() PingPong {
    var pingpong = PingPong.new(1024,768);
    pingpong.set_shader(
        &\\ #version 300 es
        \\ precision highp float;
        \\ uniform sampler2D uTex0;
        \\ in vec2 vUv;
        \\ uniform vec2 resolution;
        \\ out vec4 glFragColor;
        \\ void main(){
        \\    vec2 uv = gl_FragCoord.xy / resolution;
        \\    vec4 dat = texture(uTex0,uv);
        \\    glFragColor = vec4(1.0,1.0,0.0,1.0);
        \\}
        [0]
    );

    return pingpong;
}

fn build_renderplane() obj.DisplayObject {
    var verts2 = Vbo.new();
    verts2.add_vertices(geo.FULLSCREEN_TRIANGLE);
    verts2.set_size(2);
    verts2.compile();

    var render_plane = obj.DisplayObject.new();

    // load an alternate shader 
    var frag_shader = 
        \\ #version 300 es
        \\ precision highp float;
        \\ uniform sampler2D uTex0;
        \\ in vec2 vUv;
        \\ uniform vec2 resolution;
        \\ out vec4 glFragColor;
        \\ void main(){
        \\    vec2 uv = gl_FragCoord.xy / resolution;
        \\    vec4 dat = texture(uTex0,uv);
        \\    glFragColor = vec4(1.0,1.0,0.0,1.0);
        \\}
        ;

    render_plane.shader.load_fragment(&frag_shader[0]);
    render_plane.shader.compile();
    
    render_plane.add_attribute(verts2,0,&"position"[0]);

    return render_plane;
}


// render loop
export fn draw(_:f32) void {
    glClearColor(0,0,0,1);
    glViewportFullscreen(0,0);

    const t = &aPingPong_state;

    t.pingpong.update();
    
    t.pingpong.bindOutput(0);
    glViewportFullscreen(0,0);

    t.render_plane.bind();
    t.render_plane.shader.uniformTex(&"uTex0"[0],0);
    t.render_plane.draw(0,3);

}

// triggered when window resizes 
// 1st param is the new window width
// 2nd is the new window height 
// TODO Set to named variables,  _ in order to compile for now. 
export fn resize(_: c_int, _: c_int) void {}

//////////////////////////////////////////////

// main necessary to compile
pub fn main() !void {}