
        var reader = std.io.bufferedReader(file.reader()).reader();

        // TODO size might need to be a bit larger depending on shader. 
        var list = std.ArrayList(u8).init(std.heap.page_allocator);
        const output = try cwd.openDir("src/zig_shaders",.{});
        reader.readAllArrayList(&list,1024) catch return;

        for(list.items) | item| {
            std.debug.print("Hello {s}",.{item});
        }

        //var content = list.toOwnedSlice();
        //output.writeFile("./test.zig",content) catch return;



          // TODO size might need to be a bit larger depending on shader. 
        var list = std.ArrayList([]const u8).init(std.heap.page_allocator);
        const output = try cwd.openDir("src/zig_shaders",.{});

        list.append("pub export const PASSTHRU_FRAGMENT_SHADER = ") catch return;
        
        output.writeFile("./test.zig",list.items[0]) catch return;



        /////////

         // TODO size might need to be a bit larger depending on shader. 
        var buf:[1024]u8 = undefined;
        var content = std.ArrayList([]const u8).init(std.heap.page_allocator);
        while(try reader.readUntilDelimiterOrEof(&buf,'\n')) |_| {
            var it = std.mem.tokenize(u8,&buf,"\n");
            while(it.next()) | line | {
                content.append(line) catch return;
            }
        }

        for(content.items) | item | {
            std.debug.print("{s}",.{item});
        }


        /////////////////////////////////////////////

        const std = @import("std");

const fs = std.fs;

/// Reads a directory of shaders, converts content to zig friendly strings and 
/// outputs zig files that are usable within zig. 
pub fn main() !void {
  
     const cwd = fs.cwd();

    // make directory and enable for iteration
    var options = fs.Dir.OpenDirOptions{
        .iterate = true
    };
    const dir = try cwd.openDir("src/shaders",options);
    
    var w = try dir.walk(std.heap.page_allocator);
    defer w.deinit();

    // loop through all the files. 
    while(try w.next()) | entry |{
        //std.debug.print("Hello {s}",.{entry.path});
        var file = try dir.openFile(entry.path,.{});
        defer file.close();

        const start = "pub export const OUTPUT =";
        const end = "\n;";

        // get file size
        const file_size = (try file.stat()).size;
        const total_size = start.len + file_size + end.len;
        
        var buffer = try std.heap.page_allocator.alloc(u8,total_size);

        empty(buffer);

        // fill start 
        var i: usize = 0;
        while (i < start.len) : (i += 1) {
            buffer[0 + i] = start[i];
        }

        // fill file contents 
        //var fbuffer = try std.heap.page_allocator.alloc(u8,file_size);
        //try file.reader().readNoEof(fbuffer);
        const literal = "\n \\\\ als;fiehosif";
        i = 0;
        while (i < literal.len) : (i += 1) {
            buffer[start.len + i] = literal[i];
        }

        // fill end 
        i = 0;
        while (i < end.len) : (i += 1) {
            buffer[start.len + literal.len + i] = end[i];
        }


        const output = try cwd.openDir("src/zig_shaders",.{});
        try output.writeFile("./test.zig", buffer);
    
    }
 
    //std.debug.print("Hello {}",.{w.stack.items.len});

}

/// empties string buffer and replaces nulls with newlines
fn empty(buffer:[]u8) void{
    var len:usize = buffer.len;
    var i:usize = 0;

    while(i < len) : (i += 1){
        buffer[i] = "\n"[0];
    }
}
