

function checkError(gl,shader,shaderTypeName){
    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        console.error(`Error in ${shaderTypeName} ` + gl.getShaderInfoLog(shader), true);

        // just halt execution if we run into an error.
        throw new Error("Shader error - see message");

    }
}


/**
 * Barebones shader compiler
 * @param gl {WebGL2RenderingContext} a webgl 2 rendering context
 * @param vertex {String} shader source for vertex
 * @param fragment {String} shader source for fragment
 * @param program {WebGLProgram} the webgl program to attach the shaders to.
 */
export function compileShader(gl,vertex,fragment,program){

    ////// CHECK VERTEX SHADER ////////
    let vertex_shader = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertex_shader, vertex);
    gl.compileShader(vertex_shader)

    checkError(gl,vertex_shader,"vertex shader")

    ////// CHECK FRAGMENT SHADER ////////
    let fragment_shader = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragment_shader, fragment);
    gl.compileShader(fragment_shader)

    checkError(gl,fragment_shader,"fragment shader")

    gl.attachShader(program,vertex_shader);
    gl.attachShader(program,fragment_shader);
    gl.linkProgram(program);

    gl.deleteShader(vertex_shader);
    gl.deleteShader(fragment_shader);

    program.vertex_source = vertex;
    program.fragment_source = fragment;


    if(!gl.getProgramParameter(program,gl.LINK_STATUS)){
        console.error("Could not initialize WebGL program");
        throw ("Couldn't link shader program - " + gl.getProgramInfoLog(this));
    }else{
        program.linked = true;
    }
}