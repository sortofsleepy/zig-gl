
// global data store of GL objects. 
// Should pass index of object to zig so each object 
// knows what gl object it's associated with. 
export default {
    buffers:[],
    textures:[],
    framebuffers:[],
    vaos:[],
    shaders:[],
    settings:{
        clearColor:[0,0,0,1]
    }
}