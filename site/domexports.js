
import {memoryAddressToString,memoryAddressToFloat32Array} from "./utils.js"

export default function(){

    const getWindowWidth = () => {
        return window.innerWidth
    }

    const getWindowHeight = () => {
        return window.innerHeight;
    }

    /// Variadic, will joing subsequent arguments with pipe 
    const logValue = (...val) => {
        console.log(val.join(" | "));
    }

    return {
        getWindowWidth,
        getWindowHeight,
        logValue
    }
}