
import getGLExports from "./glexports.js"
import getDomExports from "./domexports"
import data from "./data.js"

(async function(){
    // setup a context 
    const canvas = document.querySelector("#canvas");
    const gl = canvas.getContext("webgl2");


    // will hold callbacks we can call from Zig. 
    const env = {}

    // store object state of things like buffers and textures.
    const state = data;

    // setup canvas and attach to body
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    document.body.appendChild(canvas);

    // build exports to Zig
    const glExports = getGLExports(gl,state);
    Object.assign(env,glExports)

    const domExports = getDomExports();
    Object.assign(env,domExports);

    let start = Date.now();

    // load module
    // TODO add loading indicator. 
    WebAssembly.instantiateStreaming(fetch("./zig_gl.wasm"),{env})
    .then(results => {
       
        let instance = results.instance;
        let funcs = instance.exports;
   
        window.memory = results.instance.exports.memory;
       
        funcs.setup();

        
        const animate = function(ts){
            let time = (Date.now() - start) * 0.001;
            requestAnimationFrame(animate);  
            funcs.draw(time)
        }

        animate();
        
        window.addEventListener("resize",()=> {
            funcs.resize(window.innerWidth,window.innerHeight);
            canvas.width = window.innerWidth;
            canvas.height = window.innerHeight
        });        
    }).catch(e => {
        console.error(e);
    })

})()// end async