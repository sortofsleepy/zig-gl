

/**
 * Converts a WASM address to native JS value. 
 * Assumes JS value is a string
 * @param {} memory wasm memory object.
 * @param {number} address address of the string. 
 * @returns 
 */
export function memoryAddressToString(memory,address){
    
    let buffer = new Uint8Array(memory.buffer,address,memory.buffer.byteLength - address);
    let term = buffer.indexOf(0);
    let text = new TextDecoder().decode(buffer.subarray(0,term));
    return text;
}

/**
 * 
 * @param {} memory WASM memory object 
 * @param {number} address The address to the data in the wasm package
 * @param {number} length The length of the expected array
 */
export function memoryAddressToFloat32Array(memory,address,length){
    return new Float32Array(memory.buffer, address, length);
}