
const Builder = @import("std").build.Builder;
const glfw = @import("libs/mach-glfw/build.zig");
const std = @import("std");

pub fn build(b: *Builder) void {

    //// BUILD SHADER IMPORTER /////
    const target = b.standardTargetOptions(.{});

    // Standard release options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall.
    const mode = b.standardReleaseOptions();

    const shd_exe = b.addExecutable("shader_compiler", "src/shader_compiler.zig");
    shd_exe.setTarget(target);
    shd_exe.setBuildMode(mode);
    shd_exe.install();

    ///// RUN SHADER COMPILER /////
    const run_step = shd_exe.run();
    const step = b.step("run_shader_compiler", "Runs executable");
    step.dependOn(&run_step.step);

    ////// BUILD WASM ///// 
    //https://github.com/ziglang/zig/issues/8633
    const exe = b.addExecutable("zig_gl", "src/main.zig");
    exe.setTarget(.{.cpu_arch = .wasm32, .os_tag = .freestanding});
    exe.setOutputDir("site");

    //exe.import_memory = true;
    //exe.initial_memory =  65536 * 4240;
    //exe.max_memory =  65536 * 4240;
    //exe.global_base = 6560;
    ///// RUN //////

    // make sure wasm output depends on shader compiler running. 
    exe.step.dependOn(step);

    // finally build wasm.
    b.default_step.dependOn(&exe.step);

}